from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.post import Post
from .models.reaction import Reaction
from .models.follower import Follower
from .models.following import Following
from .models.typepet import TypePet
from .models.typepost import TypePost
from.models.typereaction import TypeReaction
# Register your models here.

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Post)
admin.site.register(Reaction)
admin.site.register(Follower)
admin.site.register(Following)
admin.site.register(TypePet)
admin.site.register(TypePost)
admin.site.register(TypeReaction)