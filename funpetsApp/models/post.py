from django.db import models

from .account import Account
from .typepost import TypePost

class Post(models.Model):
    post_id = models.BigAutoField(primary_key=True)
    post_creation_date = models.DateTimeField()
    post_text = models.CharField('Post_text', null=True, max_length = 256)
    post_image_ref = models.CharField('Post_image_ref', null=True, max_length = 256)
    post_video_ref = models.CharField('Post_video_ref', null=True, max_length = 256)
    post_gif_ref = models.CharField('Post_gif_ref', null=True, max_length = 256)

    post_account_id = models.ForeignKey(Account, on_delete=models.CASCADE)
    post_typepost_id = models.OneToOneField(TypePost, on_delete=models.CASCADE)
    post_parent_id = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    