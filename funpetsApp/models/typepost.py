from django.db import models

class TypePost(models.Model):
    typepost_id = models.AutoField(primary_key=True)
    typepost_name = models.CharField('TypePost_name', max_length = 100)
    