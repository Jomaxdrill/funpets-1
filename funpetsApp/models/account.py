from django.db import models
from .user import User
from .typepet import TypePet
class Account(models.Model):
    account_id = models.AutoField(primary_key=True)
    account_name=models.CharField('Account_name', max_length = 256)
    account_nickname=models.CharField('Account_nickname', max_length = 256)
    account_image=models.CharField('Account_image',null=True, max_length = 500)
    account_description=models.TextField('Account_description',null=True,max_length=300)
    account_birthdate = models.DateTimeField(null=True)
    account_creationdate = models.DateTimeField()
    user_id_account = models.ForeignKey(User,on_delete=models.CASCADE)
    account_typepet_id=models.OneToOneField(TypePet, on_delete=models.CASCADE)
    