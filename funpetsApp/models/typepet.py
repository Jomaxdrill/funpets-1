from django.db import models

class TypePet(models.Model):
    typepet_id = models.AutoField(primary_key=True)
    typepet_name = models.CharField('TypePet_name', max_length = 100)
    