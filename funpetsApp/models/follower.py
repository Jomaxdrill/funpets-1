from django.db import models
from .user import User
from .account import Account

class Follower(models.Model):
    follower_id=models.BigAutoField(primary_key=True)
    follower_creation_date = models.DateTimeField()
    account_id_follower = models.OneToOneField(Account, on_delete = models.CASCADE)
    user_id_follower = models.OneToOneField(User, on_delete = models.CASCADE)
    

