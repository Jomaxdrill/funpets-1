from django.db import models
from .user import User
from .account import Account

class Following(models.Model):
    following_id=models.BigAutoField(primary_key=True)
    following_creation_date = models.DateTimeField()
    user_id_following = models.OneToOneField(User, on_delete = models.CASCADE)
    account_id_following = models.OneToOneField(Account, on_delete = models.CASCADE)
    

